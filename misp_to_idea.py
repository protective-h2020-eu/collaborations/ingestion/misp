#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, CESNET, z. s. p. o.
# Use of this source is governed by an ISC license, see LICENSE file.

import os
import sys
import json
import argparse
import resource
import atexit
from datetime import datetime
import signal
import logging
import csv
import time

from pymisp import ExpandedPyMISP

from misp import MispToIdea
from misp import category_to_taxonomy

misp_wanted_tags = []
for misp_tag_list in category_to_taxonomy.values():
    misp_wanted_tags += misp_tag_list

# flag for the conversion completion in case of exiting the script
conversion_active_flag = False
# when script is about to exit, switch to False
running_flag = True
# save ids of events, which were converted, because the script can query data based on date and if it will query every
# 20 minutes, it would convert same events multiple times, so save ids which were already converted and omit them from
# conversion
converted_events = []
downloaded_all_events = False

logger = logging.getLogger("misp_to_idea.py")

tlp_defined_levels = ["tlp:white", "tlp:green", "tlp:amber", "tlp:red"]


def daemonize(uid=None, gid=None, pidfile=None, signals={}):
    """
    Daemonizes the process
    """
    if gid is not None:
        os.setgid(gid)
    if uid is not None:
        os.setuid(uid)
    # Doublefork, split session
    if os.fork() > 0:
        os._exit(0)
    try:
        os.setsid()
    except OSError:
        pass
    if os.fork() > 0:
        os._exit(0)
    # Setup signal handlers
    for (signum, handler) in signals.items():
        signal.signal(signum, handler)
    # Close descriptors
    maxfd = resource.getrlimit(resource.RLIMIT_NOFILE)[1]
    if maxfd == resource.RLIM_INFINITY:
        maxfd = 65535
    for fd in range(maxfd, 3, -1):  # 3 means omit stdin, stdout, stderr
        try:
            os.close(fd)
        except Exception:
            pass
    # Redirect stdin, stdout, stderr to /dev/null
    devnull = os.open(os.devnull, os.O_RDWR)
    for fd in range(3):
        os.dup2(devnull, fd)
    # PID file
    if pidfile is not None:
        pidd = os.open(pidfile, os.O_RDWR | os.O_CREAT | os.O_EXCL | os.O_TRUNC)
        os.write(pidd, (str(os.getpid())+"\n").encode('utf-8'))
        os.close(pidd)
        # Define and setup atexit closure
        @atexit.register
        def unlink_pid():
            try:
                os.unlink(pidfile)
            except Exception:
                pass


def get_args():
    """
    Create argument parser and initialize all possibly used arguments
    :return: argument parser
    """
    parser = argparse.ArgumentParser(
        description="Converter script, which loads MISP events from MISP instance and converts them to IDEA format and "
                    "saves them to files.")
    parser.add_argument("--path", required=True, dest="idea_path", action="store",
                        help="Path to folder, where the IDEA events will be saved to")
    parser.add_argument("--url", required=True, dest="misp_url", action="store",
                        help="URL to MISP instance")
    parser.add_argument("--key", required=True, dest="misp_key", action="store",
                        help="MISP instance API key needed for authentication and for downloading events from MISP "
                             "instance")
    parser.add_argument("--cert", required=False, dest="misp_cert", action="store", default=None,
                        help="Path to optional MISP instance certificate")
    parser.add_argument("--test", required=False, dest="test", action="store_true", default=False,
                        help="Add Test Tag to converted MISP event")
    parser.add_argument("--poll", required=False, dest="poll_interval", action="store", default=20,
                        help="Select polling interval, which will be used to poll MISP instance (minutes) (max. 59)")
    parser.add_argument("--unpublished", required=False, dest="unpublished", action="store_true", default=False,
                        help="Download unpublished events from MISP instance (default converts only published events)")
    parser.add_argument("--daemonize", required=False, dest="daemonize", action="store_true", default=False,
                        help="Run script as daemon")
    parser.add_argument("--pid",
                        default=os.path.join("/var/run", os.path.splitext(os.path.basename(sys.argv[0]))[0] + ".pid"),
                        dest="pid", action="store", help="Create PID file with this name"),
    parser.add_argument("--uid", default=None, dest="uid", action="store", help="User id to run under")
    parser.add_argument("--gid", default=None, dest="gid", action="store", help="Group id to run under")
    parser.add_argument("--log", dest="log_dest", action="store", default="misp_to_idea.log",
                        help="Destination file of log messages")
    parser.add_argument("--verbose", required=False, dest="verbose", default=False, action="store_true",
                        help="Set logging level to DEBUG")
    parser.add_argument("--tlp", required=False, dest="tlp", default="green", action="store",
                        help="Set tlp level of events, which will be downloaded from MISP instance. Events with the "
                             "same or lower level will be then downloaded. Pass only the value (white, green etc.)")
    parser.add_argument("--all_events", required=False, dest="all_events", default=False, action="store_true",
                        help="Download all events from MISP instance, then continue with downloading and converting"
                             " only new events (default: downloads only new events)")
    parser.add_argument("--org", required=False, dest="org_name", default=-1, action="store",
                        help="Name of your organization, under which will the connector download event (used to exclude"
                             "events from your organization)")
    return parser


def save_event(idea_event, directory_path):
    """
    Saves newly converted event to new IDEA file
    :param idea_event: data to be saved (json)
    :param directory_path: path to directory, where the new file will be saved
    :return: None
    """
    with open(os.path.join(directory_path, idea_event['ID'] + ".idea"), 'w') as new_idea_file:
        json.dump(idea_event, new_idea_file)


def get_dates(poll_interval):
    """
    Get date_from and date_to based on current time - poll_interval.
    Date_from and Date_to are always same, only in one case they differ and it's on the turn of the day
    :param poll_interval:
    :return:
    """
    current_date = datetime.today()
    # if current time is after midnight, check if the date from should not be yesterday
    if current_date.hour == 0 and current_date.minute < poll_interval:
        date_from = current_date.replace(day=current_date.day-1).strftime("%Y-%m-%d")
    else:
        date_from = current_date.strftime("%Y-%m-%d")

    date_to = current_date.strftime("%Y-%m-%d")

    return date_from, date_to


def get_todays_date_converted_events_filename():
    """
    Gets name of file, which holds ids of events, which were converted today
    :return: the filename
    """
    todays_date = datetime.today().strftime("%Y_%m_%d")

    # check if there were not saved ids of converted events, if yes, load the ids
    return "misp_to_idea_converted_" + todays_date + ".txt"


def is_correct_tlp_level(misp_event, tlp_level):
    """
    Checks whether the TLP level of misp event is correct (same, lower or None)
    :param misp_event: checked misp event
    :param tlp_level: checked tlp level
    :return: True if correct TLP level, otherwise false
    """
    for tag in misp_event['Tag']:
        if tag['name'].startswith("tlp"):
            if tlp_defined_levels.index(tag['name']) <= tlp_defined_levels.index(tlp_level):
                # if the same or lower level, it's ok to share
                return True
            # else do not share
            return False
    # if no tlp level tag, it is ok to share freely
    return True


def get_events(misp_inst, converter, directory_path, poll_interval, unpublished=False, test=False,
               tlp_level="tlp:green", download_all_events=False, org_id=-1):
    """
    Downloads events from today and converts them if they weren't already
    :param misp_inst: Connection to MISP instance (ExpandedPyMISP instance)
    :param converter: Instance of converter MISP to IDEA
    :param directory_path: Path to directory, where will be the converted events saved
    :param poll_interval: Polling interval used to check if new day hasn't come
    :param unpublished: Download unpublished events too
    :param test: Add test Category to converted IDEA message
    :param tlp_level: Set tlp level of events, which should be downloaded from MISP instance
    :param download_all_events: Download all events, not just from today
    :param org_id: Id of organization, which will be excluded from pull query
    :return: updated list of converted events (their ids)
    """
    global conversion_active_flag, running_flag, converted_events, downloaded_all_events
    date_from, date_to = get_dates(poll_interval)
    remove_ids = []
    # if dates differ, then after conversion remove ids of converted events from yesterday
    if date_from != date_to:
        remove_ids = converted_events

    # 2 = Connected Communities
    # 3 = All Communities
    # 4 = Sharing Group
    # 5 = Inherit event (inherits distribution level of event)
    wanted_distribution_level = [2, 3, 4, 5]

    # normally i would pass arguments in the dictionary, but kwargs in search function are not working as expected, so
    # it has to be split to this not nice looking double layer if
    if download_all_events and not downloaded_all_events:
        # in the first query download all events
        if not unpublished:
            # query only published events
            # looks like [{Event: {event data...}}, {Event: {event data...}}]
            logger.debug("Downloading all (published) events ...")
            downloaded_events = misp_inst.search(controller='events', published=True, tags=misp_wanted_tags,
                                                 distribution=wanted_distribution_level, org={"NOT": org_id})
        else:
            # query published and unpublished events
            logger.debug("Downloading all events (including unpublished) ...")
            downloaded_events = misp_inst.search(controller='events', tags=misp_wanted_tags,
                                                 distribution=wanted_distribution_level, org={"NOT": org_id})
        downloaded_all_events = True
    else:
        # in the second and more query download only new events
        if not unpublished:
            logger.debug("Downloading (published) events between {} and {} ...".format(date_from, date_to))
            downloaded_events = misp_inst.search(controller='events', published=True, date_from=date_from, date_to=date_to,
                                                 tags=misp_wanted_tags, distribution=wanted_distribution_level,
                                                 org={"NOT": org_id})
        else:
            logger.debug("Downloading events (including unpublished) between {} and {} ...".format(date_from, date_to))
            downloaded_events = misp_inst.search(controller='events', date_from=date_from, date_to=date_to,
                                                 tags=misp_wanted_tags, distribution=wanted_distribution_level,
                                                 org={"NOT": org_id})

    if running_flag:
        conversion_active_flag = True

        logger.info("Downloaded {count} MISP events from MISP instance!".format(count=len(downloaded_events)))
        logger.info("Next query to MISP instance for new MISP events will be triggered off in {poll} minutes from "
                     "now!".format(poll=poll_interval))

        for misp_event in downloaded_events:
            misp_event = misp_event['Event']
            # check tlp level
            if not is_correct_tlp_level(misp_event, tlp_level):
                logger.info("Event {id} was not converted, because it has higher tlp level than {tlp}!".format(
                    id=misp_event['id'], tlp=tlp_level))
                continue
            # if event wasn't already converted, convert it
            if misp_event['id'] not in converted_events:
                idea_data = None
                try:
                    idea_data = converter.to_idea(misp_event, test=test, misp_url=misp_inst.root_url)
                except Exception:
                    logger.error("Something went wrong while converting MISP event {id} to IDEA!".format(
                        id=misp_event['id']), exc_info=True)

                if not idea_data:
                    continue
                converted_events.append(misp_event['id'])

                try:
                    save_event(idea_data, directory_path)
                    logger.info("Converted MISP event {id} was successfully saved to {filename}!".format(
                        id=misp_event['id'], filename=idea_data['ID'] + ".idea"))
                except Exception:
                    logger.error("MISP event {id} was successfully converted to IDEA, but cannot be saved to "
                                 "file!".format(id=misp_event['id']), exc_info=True)
            else:
                logger.debug("MISP event {id} will not be converted, because it was already once converted today! "
                             "If you want to convert it anyways, stop the connector, delete the file ".format(id=misp_event['id']) +
                             get_todays_date_converted_events_filename() + " (located in the same folder "
                             "as the connector is) and run the connector again!".format(id=misp_event['id']))
        conversion_active_flag = False

    # if it is the turn of the day, remove ids of yesterday events
    if date_from != date_to:
        converted_events = [event_id for event_id in converted_events if event_id not in remove_ids]


def terminate_me(signum, frame):
    """
    Sets running flag to fals and terminates process, waits until any conversion finishes
    :return: None
    """
    global conversion_active_flag, converted_events, running_flag
    running_flag = False
    while True:
        if not conversion_active_flag:
            # if script exits, save ids of events, which were converted in case, it will turn on in the same day
            # (otherwise these events will be ignored and cleared on day turn)
            todays_date_filename = get_todays_date_converted_events_filename()
            with open(todays_date_filename, 'w') as converted_events_file:
                writer = csv.writer(converted_events_file)
                writer.writerow(converted_events)
            logging.debug("Exiting the connector, ids of converted events was saved into {filename}!".format(
                filename=todays_date_filename))
            sys.exit()


def load_converted_events():
    """
    Loads converted events ids if the file exists, otherwise just create empty list. If there are some old files, delete
    them
    :return: None
    """
    global converted_events

    files_in_dir = os.listdir(os.getcwd())

    # check if there were not saved ids of converted events, if yes, load the ids
    converted_events_filename = get_todays_date_converted_events_filename()
    if converted_events_filename in files_in_dir:
        with open(converted_events_filename) as converted_events_file:
            reader = csv.reader(converted_events_file)
            for ids in reader:
                converted_events = ids
                break
    else:
        converted_events = []

    # delete old files, which holds information about converted events
    for filename in files_in_dir:
        if "misp_to_idea_converted_" in filename:
            os.remove(os.path.join(os.getcwd(), filename))


def main():
    arguments = get_args().parse_args()

    # check poll interval, if it makes sense
    if int(arguments.poll_interval) > 59 or int(arguments.poll_interval) < 1:
        sys.exit("Poll interval has to be between 1 to 59")

    # check tlp argument
    if arguments.tlp not in ("white", "green", "amber", "red"):
        sys.exit("TLP value has to be on of these: (white, green, amber, red)!")
    else:
        arguments.tlp = "tlp:" + arguments.tlp

    logging_level = logging.DEBUG if arguments.verbose else logging.INFO
    # configure logger
    logging.basicConfig(filename=arguments.log_dest, filemode='w',
                        format='%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', level=logging_level)

    try:
        misp_inst = ExpandedPyMISP(url=arguments.misp_url, key=arguments.misp_key, cert=arguments.misp_cert)
    except Exception:
        logger.error("Cannot connect to MISP instance {url}!".format(url=arguments.misp_url), exc_info=True)
        sys.exit()

    converter = MispToIdea()

    if arguments.daemonize:
        try:
            daemonize(
                pidfile=arguments.pid,
                uid=arguments.uid,
                gid=arguments.gid,
                signals={
                    signal.SIGINT: terminate_me,
                    signal.SIGTERM: terminate_me,
                })
        except Exception:
            logger.error("Something went wrong while trying to daemonize", exc_info=True)
            sys.exit()
    else:
        signal.signal(signal.SIGINT, terminate_me)
        signal.signal(signal.SIGTERM, terminate_me)

    # converted events list needed to prevent conversion of events, which was already converted this day
    try:
        load_converted_events()
    except Exception:
        logger.error("Cannot load ids of already converted events today!", exc_info=True)
        sys.exit()

    # try to get id of the organization, which is using the connector, to prevent pulling their events back
    org_id = arguments.org_name
    if org_id != -1:
        try:
            # get list of all organizations and find id of the organization
            org_list = misp_inst.get_organisations_list()
            for org in org_list:
                if org['Organisation']['name'] == arguments.org_name:
                    org_id = org['Organisation']['id']
        except Exception:
            pass
        # if id not found --> exit
        if org_id == -1:
            logger.error("Cannot connect to MISP instance!", exc_info=True)
            sys.exit()

    # query misp instance for new events once every x (poll_interval) minutes
    while running_flag:
        get_events(misp_inst, converter, arguments.idea_path, arguments.poll_interval, arguments.unpublished,
                   arguments.test, arguments.tlp, arguments.all_events, org_id)
        # poll interval divided by seconds, otherwise exit signals would not be caught if they were used
        for i in range(arguments.poll_interval * 60):
            time.sleep(1)


if __name__ == "__main__":
    main()