FROM registry.gitlab.com/protective-h2020-eu/collaborations/core:v2

RUN mkdir /idea_files
RUN mkdir /misp-converter/logs
RUN ln -sf /dev/stdout /misp-converter/misp_to_idea.log
ADD ./misp_to_idea.py .



