# MISP

## Dockerized MISP to IDEA

Clone the repository and enter the folder: 

```
https://gitlab.com/protective-h2020-eu/collaborations/ingestion/misp.git
cd misp
```
Edit `.env` file with the URL and API Key to connect to the MISP instance from where you want to download the events.

Run `docker-compose up -d`

Then you will find the MISP events converted into IDEA format in: `./data/idea_files_from_misp` and the application logs in: `./data/logs/misp`


## Connector MISP to IDEA 

Connector is placed in **misp_to_idea.py**. The connector queries **MISP** instance every x (poll_interval) minutes, converts them to **IDEA** message and saves them to file. 

**Note:** When the script exits, it creates file **misp_to_idea_converted_YYYY_MM_DD.txt** , which contains ids of MISP converted events. This is because MISP API only allows to query events from the date and not from the date and time. So same events would be converted multiple times if these ids would not be saved (because MISP instance get queried multiple times a day) and used in next run (if the script runs on another day, the file will be ommited and deleted). If you want to still convert events multiple times (most probably only for testing purposes), stop the connector, delete the files which holds ids of converted events, and run the connector again. It will use empty list of ids then.

If any bug occurs, please try to start the connector with '--verbose' argument, it should help you to find the bug faster.

### requirements: 
* **Python3.6**
* **PyMISP** python library
* **misp** conversion library

### usage: 
```
misp_to_idea_protective.py [-h] --path IDEA_PATH --url MISP_URL --key MISP_KEY [--cert MISP_CERT] 
[--test] [--poll POLL_INTERVAL] [--daemonize] [--pid PID] [-u UID] [-g GID] [--log LOG_DEST] [--verbose]
```
#### compulsory arguments:
```
  --path IDEA_PATH      Path to folder, where the IDEA events will be saved to
  --url MISP_URL        URL to MISP instance
  --key MISP_KEY        MISP instance API key needed for authentication and
                        for downloading events from MISP instance
```
#### optional arguments:
```
  -h, --help            Shows help message
  --cert MISP_CERT      Path to optional MISP instance certificate
  --test                Add Test Tag to converted MISP event
  --poll POLL_INTERVAL  Select polling interval, which will be used to poll
                        MISP instance (minutes) (max. 59)
  --daemonize           Run script as daemon
  --pid PID             Create PID file with this name
  --uid UID             User id to run under
  --gid GID             Group id to run under
  --log LOG_DEST        Destination file of log messages
  --verbose             Set logging level to DEBUG
  --tlp TLP             Set tlp level of events, which will be downloaded from
                        MISP instance. Events with the same or lower level
                        will be then downloaded. Pass only the value (white,
                        green etc.)
  --all_events          Download all events from MISP instance, then continue
                        with downloading and converting only new events
                        (default: downloads only new events)
```
